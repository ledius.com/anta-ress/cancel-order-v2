import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {ContentPayload} from "../../resolvers/content.resolver";

@Component({
  selector: 'app-index-view',
  templateUrl: './index-view.component.html',
  styleUrls: ['./index-view.component.scss']
})
export class IndexViewComponent {

  public contentPayload: ContentPayload;

  constructor(
    private readonly route: ActivatedRoute
  ) {
    this.contentPayload = this.route.snapshot.data[0] as ContentPayload;
  }
}
