import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexViewComponent } from './index-view.component';
import {IndexModule} from "../../index/index.module";



@NgModule({
  declarations: [
    IndexViewComponent
  ],
    imports: [
        CommonModule,
        IndexModule
    ]
})
export class IndexViewModule { }
