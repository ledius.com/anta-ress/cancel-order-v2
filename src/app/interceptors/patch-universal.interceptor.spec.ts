import { TestBed } from '@angular/core/testing';

import { PatchUniversalInterceptor } from './patch-universal.interceptor';

describe('PatchUniversalInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      PatchUniversalInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: PatchUniversalInterceptor = TestBed.inject(PatchUniversalInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
