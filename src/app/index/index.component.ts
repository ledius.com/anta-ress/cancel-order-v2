import {Component, Input} from '@angular/core';
import {ContentPayload} from "../resolvers/content.resolver";
import {HttpClient} from "@angular/common/http";
import {lastValueFrom} from "rxjs";
import {ContentService} from "../services/content.service";

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
})
export class IndexComponent {

  public contact: string = '';
  public text: string = '';
  public sent: boolean = false;

  @Input()
  public contentPayload!: ContentPayload;

  constructor(
    private readonly http: HttpClient,
    private readonly contentService: ContentService,
  ) {}

  public async submit(event: Event): Promise<void> {
    event.preventDefault();

    await this.sendBid();

    await lastValueFrom(this.http.post('https://learn.ledius.ru/api/v1/mailer/send', {
      recipient: this.contentPayload.companyInformation.data.attributes.Email,
      subject: 'Заявка Судебный приказ',
      content: `<p>Контакт: ${this.contact}</p> <p>текст: ${this.text}</p>`
    }, {
      headers: {
        'Content-Type': "application/json",
        'X-Secret-Code': '626a4fbf257f5998ba30b60bed111dfa871e605585f3b6330fed0bf09dd60d44'
      }
    }));
    this.sent = true;
  }

  public async sendBid(): Promise<void> {
    try {
      await this.contentService.sendBid({Contact: this.contact, Text: this.text})
    } finally {
      this.sent = true;
    }
  }
}
