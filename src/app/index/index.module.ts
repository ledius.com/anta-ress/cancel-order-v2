import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";



@NgModule({
    declarations: [
        IndexComponent
    ],
    exports: [
        IndexComponent
    ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
  ]
})
export class IndexModule { }
