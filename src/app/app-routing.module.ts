import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IndexViewComponent} from "./views/index-view/index-view.component";
import {ContentResolver} from "./resolvers/content.resolver";

const routes: Routes = [
  {
    path: '',
    component: IndexViewComponent,
    resolve: [ContentResolver]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
