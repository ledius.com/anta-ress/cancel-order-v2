import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {lastValueFrom} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ContentService {
  constructor(
    private readonly http: HttpClient,
  ) {}

  public async fetchCompanyInformation(): Promise<ContentResponse<CompanyInformation>> {
    return await lastValueFrom(this.http.get<ContentResponse<CompanyInformation>>('/api/company-information'));
  }

  public async fetchWelcomeInformation(): Promise<ContentResponse<WelcomeInformation>> {
    return await lastValueFrom(this.http.get<ContentResponse<WelcomeInformation>>('/api/prikaz'));
  }

  public async fetchServices(): Promise<ContentListResponse<Service>> {
    return await lastValueFrom(this.http.get<ContentListResponse<Service>>('/api/uslugis'));
  }

  public async fetchQuestions(): Promise<ContentListResponse<Question>> {
    return await lastValueFrom(this.http.get<ContentListResponse<Question>>('/api/questions'));
  }

  public async sendBid(bid: Bid): Promise<ContentResponse<Bid>> {
    return await lastValueFrom(this.http.post<ContentResponse<Bid>>('/api/bids', {
      data: bid
    }));
  }
}

export interface ContentResponse<T> {
  data: {
    id: string, attributes: T
  };
}

export interface ContentListResponse<T> {
  data: {
    id: string, attributes: T
  }[];
}

export interface CompanyInformation {
  Email: string,
  Phone: string,
  Name: string,
  WhatsApp: string,
  Telegram: string
}

export interface WelcomeInformation {
  Welcome: string;
}

export interface Service {
  Title: string;
  OldPrice: string;
  Price: string;
  PriceDescription: string;
  Description: string;
}

export interface Question {
  QuestionName: string;
  Question: string;
  AnswerName: string;
  Answer: string;
}

export interface Bid {
  Contact: string;
  Text: string;
}
