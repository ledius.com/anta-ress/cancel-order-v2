import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {
  CompanyInformation,
  ContentListResponse,
  ContentResponse,
  ContentService, Question, Service,
  WelcomeInformation
} from "../services/content.service";

export interface ContentPayload {
  companyInformation: ContentResponse<CompanyInformation>;
  welcomeInformation: ContentResponse<WelcomeInformation>;
  services: ContentListResponse<Service>;
  questions: ContentListResponse<Question>;
}

@Injectable({
  providedIn: 'root'
})
export class ContentResolver implements Resolve<ContentPayload> {

  constructor(
    private readonly contentService: ContentService,
  ) {
  }

  public async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<ContentPayload> {
    const [companyInformation, welcomeInformation, services, questions] = [
      await this.contentService.fetchCompanyInformation(),
      await this.contentService.fetchWelcomeInformation(),
      await this.contentService.fetchServices(),
      await this.contentService.fetchQuestions(),
    ];

    return { companyInformation, welcomeInformation, services, questions };
  }
}
