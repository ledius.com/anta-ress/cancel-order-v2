import {Component, Inject, OnInit, PLATFORM_ID} from '@angular/core';
import Platform = NodeJS.Platform;
import {DOCUMENT} from "@angular/common";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'cancel-order-v2';

  constructor(
    @Inject(DOCUMENT)
    private readonly document: Document
  ) {
  }

  ngOnInit(): void {
    this.document.title = 'Антарес | Отмена судебного приказа';
  }
}
