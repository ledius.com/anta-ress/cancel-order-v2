FROM node:16 as node
ENV PORT 80
EXPOSE 80
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build:ssr
CMD npm run serve:ssr
